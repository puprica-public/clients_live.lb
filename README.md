cd /home/xtreamcodes/iptv_xtream_codes/wwwdir/streaming
git clone https://gitlab.com/puprica-public/clients_live.lb.git

git config --global user.email "unixforest.com@gmail.com"
git config --global user.name "unixforest"

mv clients_live.php clients_live.php.orig
echo "<?php

chdir('clients_live.lb');
include_once(\"main.php\");
" > clients_live.php

chown -R xtreamcodes.xtreamcodes *
chmod -R 0777 *

old_crons=$(crontab -l)
crontab <<EOF
$old_crons

*/8 * * * * cd /home/xtreamcodes/iptv_xtream_codes/wwwdir/streaming/clients_live.lb ; git stash ; git pull
EOF

